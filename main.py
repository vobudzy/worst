
def read_txt_file():
    file = 'file.txt'
    with open(file, 'r', encoding='utf-8') as f:
        data = f.read()
        print(data)
        return data

def write_in_file():
    with open('file.txt', 'w') as writer:
        writer.write('I love Git')
        return True


if __name__ == "__main__":
    if write_in_txt_file():
        print("[OK] write")
    
    print(read_txt_file())
    
    if delete_file():
        print("[OK] delete")

